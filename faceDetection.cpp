#include "faceDetection.h"


void detectFacesOnImage(string imageName)
{
    Mat image = imread(imageName);

    if (!image.data)
    {
        cout << "cant open an image" << endl;
        exit(-1);
    }

    CascadeClassifier facesCascadeClassifier = loadFaceClassifier();
    CascadeClassifier eyesCascadeClassifier = loadEyesClassifier();

    detectFace(image, facesCascadeClassifier, eyesCascadeClassifier);

    waitKey(0);
    destroyAllWindows();
}

void detectFaceOnVideo()
{
    VideoCapture capture(0);

	if (!capture.isOpened())
	{
		cout << "Unable to open VideoCapture" << endl;
		exit(-1);
	}

    CascadeClassifier facesCascadeClassifier = loadFaceClassifier();
    CascadeClassifier eyesCascadeClassifier = loadEyesClassifier();

    while (true)
    {
        Mat frame;
        capture >> frame;

        if (frame.empty())
        {
            break;
        }

        // na zmniejszonej klatce dzia�a p�ynniej
        //resize(frame, frame, frame.size() / 2);
        detectFace(frame, facesCascadeClassifier, eyesCascadeClassifier);

        char c = (char)waitKey(1);
        if (c == 27)
        {
            break;
        }
    } 

    capture.release();
    destroyAllWindows();
    waitKey();
}

CascadeClassifier loadFaceClassifier()
{
    CascadeClassifier facesCascadeClassifier;

    if (!facesCascadeClassifier.load("D:\\Libraries\\opencv\\sources\\data\\haarcascades\\haarcascade_frontalface_alt2.xml"))
    {
        cout << "Failed to load face CascadeClassifier!" << endl;
        exit(-1);
    }

    return facesCascadeClassifier;
}

CascadeClassifier loadEyesClassifier()
{
    CascadeClassifier eyesCascadeClassifier;

    if (!eyesCascadeClassifier.load("D:\\Libraries\\opencv\\sources\\data\\haarcascades\\haarcascade_eye.xml"))
    {
        cout << "Failed to load eyes CascadeClassifier!" << endl;
        exit(-1);
    }

    return eyesCascadeClassifier;
}

void detectFace(Mat& frame, CascadeClassifier& facesCascadeClassifier, CascadeClassifier& eyesCascadeClassifier)
{
    Mat grayFrame;
    std::vector<cv::Rect> faces;
    std::vector<cv::Rect> eyes;

    // rozpoznawanie ma odbywac si� na szarej klatce
    cvtColor(frame, grayFrame, COLOR_BGR2GRAY);
    equalizeHist(grayFrame, grayFrame); 

    // method for face detection
    facesCascadeClassifier.detectMultiScale(grayFrame, faces);

    for (size_t i = 0; i < faces.size(); i++)
    {
        string faceName = "face " + std::to_string(i + 1);

        // wyswietla wykryte twarze w osobnych oknach
        Mat faceImage = frame(faces[i]);
        resize(faceImage, faceImage, Size(200, 200));
        imshow(faceName, faceImage);

        // rysuje prostok�t na twarzy
        Rect rect_face = Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
        rectangle(frame, rect_face, Scalar(0, 200, 0), 1);

        // text nad twarza
        Point faceNamePoint = Point(faces[i].x, faces[i].y - 10);
        putText(frame, faceName, faceNamePoint, FONT_HERSHEY_TRIPLEX, 0.5, Scalar(0, 20, 200), 1);

        //rozmiar twarzy w pixelach
        string faceSizeString = to_string(faces[i].width) + "x" + to_string(faces[i].height);
        Point faceSizePoint = Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
        putText(frame, faceSizeString, faceSizePoint, FONT_HERSHEY_TRIPLEX, 0.4, Scalar(0, 20, 200), 1);

        // detecting eyes
        Mat faceROI = grayFrame(faces[i]);
        eyesCascadeClassifier.detectMultiScale(faceROI, eyes);

        for (int j = 0; j < eyes.size(); j++)
        {
            // prostokat oczy
            Rect rect_eyes = Rect(faces[i].x + eyes[j].x, faces[i].y + eyes[j].y, eyes[j].width, eyes[j].height);
            rectangle(frame, rect_eyes, Scalar(200, 0, 0));
        }

        // landmark detection
        
        
    }

    imshow("Detect", frame);
}