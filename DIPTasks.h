#pragma once

#include <iostream>
#include <stdint.h>
#include <opencv2/opencv.hpp>

void changeBrightness();


void task3();
void task4();
void task5();
void task10();
void task11(); // same as task10, but with openCV methods
void task12();
void task13();
void task14();
void task15();
void task16();
void task17();
void task18();
void task19();
void task20();
void task21();
void task22(); // masks
void task23(); // color space conversation
void task24(); // channels manipulation
void task25(); // basic thresholding
void task26(); // shadow detect
void task27(); // remove shadows from image
void task27_2(); // warp affine