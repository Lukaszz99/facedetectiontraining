#pragma once

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/dnn/dnn.hpp"
#include <iostream>

using namespace std;
using namespace cv;

void detectFaceOnVideo();
void detectFace(Mat& frame, CascadeClassifier& facesCascadeClassifier, CascadeClassifier& eyesCascadeClassifier);
void detectFacesOnImage(string imageName);
CascadeClassifier loadFaceClassifier();
CascadeClassifier loadEyesClassifier();