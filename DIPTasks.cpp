#include "DIPTasks.h"

#include <iostream>
#include <string>
#include <stdint.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void showImgAndClose(string imgName, Mat& img)
{
	imshow(imgName, img);

	waitKey(0);

	destroyAllWindows();
}

void changeBrightness()
{
	Mat img = imread("image.jpg");

	Mat res = Mat::zeros(img.size(), img.type());

	double alpha = 1.0;
	int beta = 0;

	for (int r = 0; r < img.rows; r++)
	{
		for (int c = 0; c < img.cols; c++)
		{
			for (int channel = 0; r < img.channels(); channel++)
			{
				res.at<Vec3b>(r, c)[channel] =
					saturate_cast<uchar>(alpha * img.at<Vec3b>(r, c)[channel] + beta);
			}
		}
	}

	imshow("Original", img);
	imshow("Result", res);

	waitKey(0);
	destroyAllWindows();
}

void task3()
{
	Mat img = imread("image.jpg", IMREAD_COLOR);

	Rect rect(10, 10, 100, 100);

	Mat imgWithRectangle = img(rect);

	imgWithRectangle = 0.5 * imgWithRectangle;

	imshow("Named Window", img);

	cout << img.rows << "\t" << img.cols << endl;

	waitKey(0);
}

void task4()
{
	Mat img = imread("image.jpg", IMREAD_COLOR);

	Rect rect(10, 10, 100, 100);

	Mat imgWithRectangle = img(rect).clone();

	imgWithRectangle = 0.5 * imgWithRectangle;

	imshow("Named Window", img);

	waitKey(0);
}

void task5()
{
	Mat img = imread("image.jpg", IMREAD_COLOR);
	Mat tmpImg = cv::Mat(img.size(), img.type());

	Rect rect(10, 10, 100, 100);

	Rect leftUpper(0, 0, img.cols / 2, img.rows / 2);
	Rect rightUpper(img.cols / 2, 0, img.cols / 2, img.rows / 2);
	Rect leftDown(0, img.rows / 2, img.cols / 2, img.rows / 2);
	Rect rightdown(img.cols / 2, img.rows / 2, img.cols / 2, img.rows / 2);

	img(leftUpper).copyTo(tmpImg(rightUpper));
	img(rightUpper).copyTo(tmpImg(leftDown));
	img(leftDown).copyTo(tmpImg(rightdown));
	img(rightdown).copyTo(tmpImg(leftDown));

	imshow("Window", tmpImg);

	waitKey(0);
}

void task10()
{
	Mat image1 = imread("image.jpg");
	Mat image2 = imread("image2.jpeg");

	if (image1.type() != image2.type())
	{
		image2.convertTo(image2, image1.type(), 1, 0);
	}

	Size size = (image1.size() + image2.size()) / 2;

	resize(image1, image1, size);
	resize(image2, image2, size);

	Mat result1 = Mat::zeros(size, image1.type());
	Mat result2 = Mat::zeros(size, image1.type());

	result1 = image1 + image2;
	result1 = image1 - image2;

	imshow("image 1", image1);
	imshow("image 2", image2);

	waitKey(0);
}

void task11()
{
	Mat image1 = imread("image.jpg");
	Mat image2 = imread("image2.jpeg");

	Size size = (image1.size() + image2.size()) / 2;

	Mat result1 = Mat::zeros(size, image1.type());
	Mat result2 = Mat::zeros(size, image1.type());
	Mat result3 = Mat::zeros(size, image1.type());
	Mat result4 = Mat::zeros(size, image1.type());
	Mat result5 = Mat::zeros(size, image1.type());

	image1.convertTo(image1, CV_32FC3, 1.0 / 255.0, 0);
	image2.convertTo(image2, CV_32FC3, 1.0 / 255.0, 0);

	resize(image1, image1, size);
	resize(image2, image2, size);

	//operations using openCV methods
	add(image1, image2, result1);
	normalize(result1, result1, 0, 1, NORM_MINMAX);

	subtract(image1, image2, result2);
	multiply(image1, image2, result3);
	//divide(image1, image2, result3);
	absdiff(image1, image2, result5);

	imshow("Add", result1);
	imshow("Subtract", result2);
	imshow("Multiply", result3);
	imshow("Divide", result4);
	imshow("Absolute difference", result5);

	waitKey(0);
}

void task12()
{
	Mat image1 = imread("image.jpg");
	Mat image2 = imread("image2.jpeg");

	if (image1.type() != image2.type())
	{
		image1.convertTo(image1, image2.type(), 1, 0);
	}

	Size size = image1.size();

	resize(image2, image2, size);

	Mat result1 = Mat::zeros(size, image1.type());
	Mat result2 = Mat::zeros(size, image1.type());

	Mat mask = Mat::zeros(size, CV_8U);

	circle(mask, Point(500, 200), 200, Scalar(210), -1);

	add(image1, image2, result1);
	add(image1, image2, result2, mask);

	image1.setTo(Scalar(0, 0, 255), mask);

	imshow("Without mask", result1);
	imshow("With mask", result2);
	imshow("Image1 with mask", image1);
	//imshow("Mask", mask);

	waitKey(0);
}

void task13()
{
	Mat image1 = imread("image.jpg");

	//image1.setTo(Scalar(50, 150, 250));
	//image1 = Scalar(50, 150, 250); //same result as in first
	//image1 = 255; //Image is blue, earlier was orange
	image1 = Scalar(255); //also blue
	//image1 = Scalar::all(255); //It is white, because all pixels have value of 255

	imshow("Image", image1);

	waitKey(0);
}

void task14()
{
	Mat image1 = imread("image.jpg");

	Mat mask = Mat::zeros(image1.size(), CV_8U);
	rectangle(mask, Point(200, 200), Point(290, 290), Scalar(255), -1);
	//image1.setTo(Scalar(0, 0, 255), mask);
	image1(Rect(200, 200, 100, 100)).setTo(Scalar(0, 255, 255));

	imshow("image", image1);

	waitKey(0);
}

void task15()
{
	Mat image1 = imread("image.jpg", IMREAD_GRAYSCALE);
	Mat image2 = image1.clone();

	subtract(image1, Scalar(120), image1);
	imshow("Subtract", image1);

	add(image2, Scalar(40), image2);
	imshow("Add", image2);

	waitKey(0);
}

void task16()
{
	Mat img = imread("image.jpg");

	multiply(img, Scalar(1, 1, 2), img);

	imshow("Multiply red by 2", img);

	waitKey(0);
	destroyAllWindows();
}

void task17()
{
	Mat img1 = imread("image.jpg");
	Mat img2 = imread("image2.jpeg");

	Mat res1, res2, mask;

	if (img1.type() != img2.type())
	{
		img2.convertTo(img2, img1.type());
	}

	Size size = (img1.size() + img2.size()) / 2;

	resize(img1, img1, size);
	resize(img2, img2, size);

	//addWeighted(img1, 0.4, img2, 0.6, -30, res1);
	//imshow("addWeighted", res1);

	for (int i = 0; i < 100; i++)
	{
		addWeighted(img1, 0.01 * i, img2, 1 - 0.01 * i, 0, res1);
		imshow("test", res1);
		waitKey(1);
	}
	destroyAllWindows();
	waitKey(100);
}

void task18()
{
	Mat img = imread("image.jpg", IMREAD_GRAYSCALE);

	bitwise_not(img, img);

	imshow("image", img);

	waitKey(0);
}

void task19()
{
	Mat img1 = Mat::zeros(Size(500, 500), CV_8U);
	Mat img2 = Mat::zeros(Size(500, 500), CV_8U);

	circle(img1, Point(200, 250), 100, Scalar::all(255), -1);
	circle(img2, Point(140, 150), 100, Scalar::all(255), -1);

	Mat res1, res2, res3, res4;

	bitwise_and(img1, img2, res1);
	bitwise_or(img1, img2, res2);
	bitwise_xor(img1, img2, res3);
	bitwise_and(img1, img2, res4);
	bitwise_not(res4, res4);

	imshow("img1", img1);
	imshow("img2", img2);

	imshow("And", res1);
	imshow("Or", res2);
	imshow("Xor", res3);
	imshow("Not res 1", res4);

	waitKey(0);
	destroyAllWindows();
}

void task20()
{
	Mat img1 = Mat::zeros(10, 10, CV_8U);
	Mat img2 = Mat::zeros(10, 10, CV_8U);

	img2 = Scalar(160);

	imshow("img2", img2);

	Mat mask = Mat::zeros(10, 10, CV_8U);
	mask(Rect(3, 3, 1, 1)) = 255;

	img2.copyTo(img1, mask);

	imshow("img1", img1);
	imshow("img2.1", img2);

	waitKey(0);

	destroyAllWindows();
}

void task21()
{
	Mat img = imread("image.jpg");

	cvtColor(img, img, COLOR_BGR2GRAY);

	threshold(img, img, 100, 255, THRESH_BINARY);

	imshow("img", img);

	waitKey(0);

	destroyAllWindows();
}

void task22()
{
	Mat img1 = imread("image2.jpeg");
	Mat res1, res2;

	resize(img1, img1, Size(500, 500));

	Mat mask = Mat::zeros(Size(500, 500), CV_8U);
	circle(mask, Point(250, 150), 120, Scalar(255), -1);

	// making image with mask
	bitwise_and(img1, img1, res1, mask);

	// making operations with threshold
	Mat img2 = img1.clone();

	cvtColor(img2, img2, COLOR_BGR2GRAY);

	threshold(img2, img2, 100, 255, THRESH_BINARY);

	bitwise_and(img1, img1, res2, img2);

	imshow("img", res1);
	imshow("img2", img2);
	imshow("img3", res2);

	waitKey(0);

	destroyAllWindows();
}

void task23()
{
	Mat img = imread("image.jpg");
	Mat hsv = Mat(img.size(), img.type());
	Mat hsl = Mat(img.size(), img.type());
	Mat xyz = Mat(img.size(), img.type());
	Mat lab = Mat(img.size(), img.type());

	cvtColor(img, hsv, COLOR_BGR2HSV);
	cvtColor(img, hsl, COLOR_BGR2HLS);
	cvtColor(img, xyz, COLOR_BGR2XYZ);
	cvtColor(img, lab, COLOR_BGR2Lab);


	imshow("BGR", img);
	imshow("HSV", hsv);
	imshow("HSL", hsl);
	imshow("XYZ", xyz);
	imshow("Lab", lab);

	waitKey(0);
	destroyAllWindows();
}

void task24()
{
	Mat img = imread("image.jpg");
	Mat hsv = Mat(img.size(), img.type());
	Mat hsl = Mat(img.size(), img.type());
	Mat xyz = Mat(img.size(), img.type());
	Mat lab = Mat(img.size(), img.type());

	cvtColor(img, hsv, COLOR_BGR2HSV);
	cvtColor(img, hsl, COLOR_BGR2HLS);
	cvtColor(img, xyz, COLOR_BGR2XYZ);
	cvtColor(img, lab, COLOR_BGR2Lab);

	std::vector<cv::Mat> imgChannels;

	split(img, imgChannels);
	imshow("First channel", imgChannels[0]);
	imshow("Second channel", imgChannels[1]);
	imshow("Third channel", imgChannels[2]);
	waitKey(0);

	split(hsv, imgChannels);
	imshow("First channel", imgChannels[0]);
	imshow("Second channel", imgChannels[1]);
	imshow("Third channel", imgChannels[2]);
	waitKey(0);

	split(hsl, imgChannels);
	imshow("First channel", imgChannels[0]);
	imshow("Second channel", imgChannels[1]);
	imshow("Third channel", imgChannels[2]);
	waitKey(0);

	split(xyz, imgChannels);
	imshow("First channel", imgChannels[0]);
	imshow("Second channel", imgChannels[1]);
	imshow("Third channel", imgChannels[2]);
	waitKey(0);

	split(lab, imgChannels);
	imshow("First channel", imgChannels[0]);
	imshow("Second channel", imgChannels[1]);
	imshow("Third channel", imgChannels[2]);
	waitKey(0);


	destroyAllWindows();
}

void task25()
{
	Mat img = imread("image.jpg");
	Mat grayImg;

	cvtColor(img, grayImg, COLOR_BGR2GRAY);

	Mat binary, binary_inv, otsu, trunc;

	threshold(grayImg, binary, 100, 255, THRESH_BINARY);
	threshold(grayImg, otsu, 100, 255, THRESH_OTSU);
	threshold(grayImg, trunc, 100, 255, THRESH_TRUNC + THRESH_BINARY_INV);

	imshow("bnary", binary);
	imshow("otsu", otsu);
	imshow("trunc", trunc);

	waitKey(0);
}

void task26()
{
	Mat img = imread("image2.jpeg");

	cvtColor(img, img, COLOR_BGR2HSV);

	vector<Mat> hsvCh;

	split(img, hsvCh);

	normalize(hsvCh[1], hsvCh[1], 0, 255, NORM_MINMAX);
	normalize(hsvCh[2], hsvCh[2], 0, 255, NORM_MINMAX);

	Mat shadowMask = hsvCh[1] - hsvCh[2];

	threshold(shadowMask, shadowMask, 20, 255, THRESH_BINARY);

	imshow("shadow", shadowMask);

	waitKey(0);
}

void task27()
{
	Mat img = imread("image3.jpg");

	cvtColor(img, img, COLOR_BGR2HSV);

	vector<Mat> hsvCh;

	split(img, hsvCh);

	normalize(hsvCh[1], hsvCh[1], 0, 255, NORM_MINMAX);
	normalize(hsvCh[2], hsvCh[2], 0, 255, NORM_MINMAX);

	Mat shadowMask = hsvCh[1] - hsvCh[2];

	threshold(shadowMask, shadowMask, 20, 255, THRESH_BINARY);

	erode(shadowMask, shadowMask, Mat(), Point(-1, -1), 8);

	Scalar meanSh = mean(img, shadowMask);
	Scalar meanNoSh = mean(img, Scalar::all(255) - shadowMask);
	Scalar diff = meanNoSh - meanSh;

	Mat res = img.clone();

	add(img, diff, res, shadowMask);

	cvtColor(res, res, COLOR_HSV2BGR);

	imshow("no shadow", res);

	waitKey(0);
}

void task27_2()
{
	Mat img = imread("image.jpg");

	Mat res = Mat::zeros(img.size(), img.type());

	Mat transformation = (Mat_<double>(2,3) << 1,1,0,0,1,1);

	warpAffine(img, res, transformation, res.size());

	showImgAndClose("img", res);
}