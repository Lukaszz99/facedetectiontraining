#include <iostream>
#include <stdint.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "DIPTasks.h"

#include "faceDetection.h"

using namespace std;
using namespace cv;

void takeDFT(Mat& source, Mat& destination)
{

    Mat orgComplex[2] = { source, Mat::zeros(source.size(), CV_32F) };


    Mat dftReady;
    merge(orgComplex, 2, dftReady);


    Mat dftOfOriginal;

    dft(dftReady, dftOfOriginal, DFT_COMPLEX_OUTPUT);

    destination = dftOfOriginal;
}

void recenterDFT(Mat& source)
{
    int centerX = source.cols / 2;
    int centerY = source.rows / 2;
    
    Mat q1(source, Rect(0, 0, centerX, centerY));
    Mat q2(source, Rect(centerX, 0, centerX, centerY));
    Mat q3(source, Rect(0, centerY, centerX, centerY));
    Mat q4(source, Rect(centerX, centerY, centerX, centerY));

    Mat swapMap;

    q1.copyTo(swapMap);
    q4.copyTo(q1);
    swapMap.copyTo(q4);

    q2.copyTo(swapMap);
    q3.copyTo(q2);
    swapMap.copyTo(q3);
}

void showDFT(Mat& source)
{
    Mat splitArray[2] = { Mat::zeros(source.size(), CV_32F), Mat::zeros(source.size(), CV_32F)};

    split(source, splitArray);

    Mat DFTMagnitude;

    magnitude(splitArray[0], splitArray[1], DFTMagnitude);

    DFTMagnitude += Scalar::all(1);

    log(DFTMagnitude, DFTMagnitude);

    normalize(DFTMagnitude, DFTMagnitude, 0, 1, NORM_MINMAX);

    recenterDFT(DFTMagnitude);

    imshow("DFT", DFTMagnitude);
}

void invertDFT(Mat& source, Mat& destination)
{
    Mat inverse;
    dft(source, inverse, DFT_INVERSE | DFT_REAL_OUTPUT | DFT_SCALE);

    destination = inverse;
}

void createGAussian(Size& size, Mat& output, int uX, int uY, float sigmaX, float sigmaY, float amplitude = 1.0f)
{
    Mat temp = Mat(size, CV_32F);

    for (int r = 0; r < size.height; r++)
    {
        for (int c = 0; c < size.width; c++)
        {
            float x = ((c-uX) * ((float)c-uX)) / (2.0f * sigmaX * sigmaX);
            float y = ((r-uY) * ((float)r-uY)) / (2.0f * sigmaY * sigmaY);
            float value = amplitude * exp(-(x + y));

            temp.at<float>(r, c) = value;
        }
    }

    normalize(temp, temp, 0.0f, 1.0f, NORM_MINMAX);

    output = temp;
}

void showWebCam()
{
    Mat frame;

    VideoCapture vid(0);

    const int fps = 20;

    if (!vid.isOpened())
    {
        cout << "No input";
    }

    while (vid.read(frame))
    {

        imshow("Web Cam", frame);

        if (waitKey(1000 / fps) >= 0)
        {
            break;
        }
    }

}

void showAndSaveWebCam()
{
    VideoCapture cap(0);

    if (!cap.isOpened())
    {
        exit(-1);
    }

    int frameWidth = cap.get(CAP_PROP_FRAME_WIDTH);
    int frameHeight = cap.get(CAP_PROP_FRAME_HEIGHT);

    VideoWriter videoOriginal("outputWebCam.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 24, Size(frameWidth, frameHeight));
    VideoWriter videoBlue("outputWebCamBlue.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 24, Size(frameWidth, frameHeight), false);
    VideoWriter videoGreen("outputWebCamGreen.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 24, Size(frameWidth, frameHeight), false);
    VideoWriter videoRed("outputWebCamRed.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 24, Size(frameWidth, frameHeight), false);

    while (true)
    {
        Mat frame;
        cap >> frame;

        if (frame.empty())
        {
            break;
        }

        imshow("WebCam", frame);

        Mat splitChannels[3];

        split(frame, splitChannels);

        videoOriginal.write(frame);
        videoBlue.write(splitChannels[0]);
        videoGreen.write(splitChannels[1]);
        videoRed.write(splitChannels[2]);

        char c = (char)waitKey(1);
        if (c == 27)
        {
            break;
        }
    }
   
    videoBlue.release();
    videoGreen.release();
    videoRed.release();

    cap.release();

    destroyAllWindows();
}

Mat catchEdgesWithCanny(Mat& inputFrame)
{
    Mat outputFrame;

    Canny(inputFrame, outputFrame, 100, 200);

    return outputFrame;
}

void playVideo()
{
    VideoCapture cap("video.avi");

    if (!cap.isOpened())
    {
        cout << "B��d otwierania video";
        exit(1);
    }

    while (true)
    {
        Mat frame;

        cap >> frame;

        if (frame.empty())
        {
            break;
        }

        imshow("video", frame);

        char c = (char)waitKey(25);
        if (c == 27)
        {
            break;
        }
    }

    cap.release();

    destroyAllWindows();
}


int main()
{
    detectFaceOnVideo();

   // detectFacesOnImage("wesele.jpg");

    return 0; 
}